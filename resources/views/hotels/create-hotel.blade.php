@extends('layouts.app')

@section('content')
    <div class="container">
        @if (isset($message))
            <div class="col-md-12 alert-danger">
                {{ $message }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <h2 class="bg-dark text-white-50 p-3">New Hotel</h2>

                {!! Form::open(['route' => ['hotels.store']]) !!}

                {{ Form::label('hotel_name', 'Hotel Name:') }}
                {{ Form::text('hotel_name', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '15')) }}

                {{ Form::label('floor', 'Floor:', array('class' => 'mt-3')) }}
                {{ Form::text('floor', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255')) }}

                {{ Form::label('rooms_number', 'Rooms number:', array('class' => 'mt-3')) }}
                {{ Form::input('rooms_number', 'rooms_number', null, array('class' => 'form-control')) }}

                <div class="row mt-3">
                    <div class="col-sm-3">
                        {{ Form::submit('Save Changes', ['class' => 'btn btn-success btn-block']) }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>

@endsection
