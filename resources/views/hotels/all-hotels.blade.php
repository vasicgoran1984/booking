@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="col-md-4">
                    <a href="{{ route('hotels.create') }}" class="btn btn-sm btn-primary mb-4 mt-1" role="button">New Hotel</a>
                </div>
                <div class="card">
                    @if (isset($message))
                        <div class="col-md-12 alert-success">
                            {{ $message }}
                        </div>
                    @endif
                    <div class="card-header">{{ __('All Hotels') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if ($hotels->count() > 0)
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Hotel Name</th>
                                    <th scope="col">Floor</th>
                                    <th scope="col">Rooms Number</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($hotels as $hotel)
                                    <tr>
                                        <td>{{ $hotel->id }}</td>
                                        <td>{{ $hotel->hotel_name }}</td>
                                        <td>{{ $hotel->floor }}</td>
                                        <td>{{ $hotel->rooms_number }}</td>
                                        <td> <div class="col-md-3 custom">
                                                <form action="{{ route('hotels.destroy', ['hotel' => $hotel->id]) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                </form>
                                            </div></td>
                                        <td><a href="{{ route('hotels.edit', $hotel->id) }}" class="btn btn-info">Edit</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
