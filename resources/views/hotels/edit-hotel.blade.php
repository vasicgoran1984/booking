@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <h2 class="bg-dark text-white-50 p-3">Edit Hotel</h2>
                @if (isset($message))
                    <div class="col-md-12 alert-danger">
                        {{ $message }}
                    </div>
                @endif
                {!! Form::model($hotel, ['route' => ['hotels.update', $hotel->id], 'method' => 'PUT']) !!}

                {{ Form::label('hotel_name', 'Hotel Name:') }}
                {{ Form::text('hotel_name', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '15')) }}

                {{ Form::label('floor', 'Floor:', array('class' => 'mt-3')) }}
                {{ Form::text('floor', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255')) }}

                {{ Form::label('rooms_number', 'Rooms number:', array('class' => 'mt-3')) }}
                {{ Form::input('rooms_number', 'rooms_number', null, array('class' => 'form-control')) }}

                <div class="row mt-3">
                    <div class="col-sm-3">
                        {!! Html::linkRoute('hotels.show', 'Cancel', array($hotel->id), array('class'=> 'btn btn-danger btn-block')) !!}
                    </div>
                    <div class="col-sm-3">
                        {{ Form::submit('Save Changes', ['class' => 'btn btn-success btn-block']) }}
                    </div>
                </div>
            </div>
        </div>

@endsection
