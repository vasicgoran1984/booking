@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @if (isset($message))
                <div class="col-md-12 alert-info">
                    {{ $message }}
                </div>
            @endif
            <div class="col-md-12 ">

                <h2>{{ $hotel['hotel_name'] }}</h2>

                <hr/>
            </div>
            <div class="col-md-4 border bg-dark text-white-50">
                <div class="well mt-3">

                    <dl class="dl-horizontal">
                        <label>Title:</label>
                        <p class="text-white">{{ $hotel->floor }}</p>
                    </dl>

                    <dl class="dl-horizontal">
                        <label>Title:</label>
                        <p class="text-white">{{ $hotel->floor }}</p>
                    </dl>

                    <div class="row mb-3">
                        <div class="col-sm-8">
                            {!! Html::linkRoute('hotels.edit', 'Edit', array($hotel->id), array('class'=> 'btn btn-primary btn-block')) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
