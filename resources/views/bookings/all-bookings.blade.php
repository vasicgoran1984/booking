@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="col-md-4">
                    <a href="{{ route('bookings.create') }}" class="btn btn-sm btn-primary mb-4 mt-1" role="button">New Booking</a>
                </div>
                <div class="card">
                    @if (isset($message))
                        <div class="col-md-12 alert-success">
                            {{ $message }}
                        </div>
                    @endif
                    <div class="card-header">{{ __('All Bookings') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if ($bookings->count() > 0)
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Room Name</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Start Date</th>
                                    <th scope="col">End Date</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($bookings as $booking)
                                    <tr>
                                        <td>{{ $booking->id }}</td>
                                        <td>{{ $booking->room_name }}</td>
                                        <td>{{ $booking->title }}</td>
                                        <td>{{ $booking->start_date }}</td>
                                        <td>{{ $booking->end_date }}</td>
                                        <td> <div class="col-md-3 custom">
                                                <form action="{{ route('bookings.destroy', ['booking' => $booking->id]) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                </form>
                                            </div></td>
                                        <td><a href="{{ route('bookings.edit', $booking->id) }}" class="btn btn-info">Edit</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
