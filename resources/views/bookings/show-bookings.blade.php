@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @if (isset($message))
                <div class="col-md-12 alert-info">
                    {{ $message }}
                </div>
            @endif
        <div class="col-md-12 ">

            <h2>{{ $booking['room_name'] }}</h2>

            <hr/>
        </div>
        <div class="col-md-4 border bg-dark text-white-50">
            <div class="well mt-3">

                <dl class="dl-horizontal">
                    <label>Title:</label>
                    <p class="text-white">{{ $booking->title }}</p>
                </dl>

                <dl class="dl-horizontal">
                    <label>Start Date:</label>
                    <p class="text-white">{{ date('M j, Y h:ia', strtotime($booking->start_date)) }}</p>
                </dl>

                <dl class="dl-horizontal">
                    <label>End Date:</label>
                    <p class="text-white">{{ date('M j, Y h:ia', strtotime($booking->end_date)) }}</p>
                </dl>
                <hr>

                <div class="row mb-3">
                    <div class="col-sm-8">
                        {!! Html::linkRoute('bookings.edit', 'Edit', array($booking->id), array('class'=> 'btn btn-primary btn-block')) !!}
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection
