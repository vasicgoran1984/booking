@extends('layouts.app')

@section('content')
    <div class="container">

<div class="row">
    <div class="col-md-8 col-md-offset-2">

        <h2 class="bg-dark text-white-50 p-3">Edit Booking</h2>
        @if (isset($message))
            <div class="col-md-12 alert-danger">
                {{ $message }}
            </div>
        @endif
        {!! Form::model($booking, ['route' => ['bookings.update', $booking->id], 'method' => 'PUT']) !!}

        {{ Form::label('room_name', 'Room Name:') }}
        {!! Form::text('room_name', null, ['class' => 'form-control input-lg']) !!}

        {{ Form::label('title', 'Title:', array('class' => 'mt-3')) }}
        {{ Form::text('title', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255')) }}

        {{ Form::label('hotel_id', 'Hotel:', array('class' => 'mt-2')) }}
        <select class="form-control" name="hotel_id">
            <option value="">Select Hotel</option>
            @foreach($hotels as $hotel)
                <option value="{{($hotel->id === $booking->hotel_id) ? $booking->hotel_id : $hotel->id}}" {{ ($hotel->id === $booking->hotel_id) ? 'selected' : ''}}>{{$hotel->hotel_name}}</option>
            @endforeach
        </select>

        {{ Form::label('start_date', 'Start Date:', array('class' => 'mt-3')) }}
        {{ Form::input('date', 'start_date', null, array('class' => 'form-control', 'min' => '2010-01-01', 'max' => '2025-12-31')) }}

        {{ Form::label('end_date', 'End Date:', array('class' => 'mt-3')) }}
        {{ Form::input('date', 'end_date', null, array('class' => 'form-control', 'min' => '2010-01-01', 'max' => '2025-12-31')) }}

        <div class="row mt-3">
            <div class="col-sm-3">
                {!! Html::linkRoute('bookings.show', 'Cancel', array($booking->id), array('class'=> 'btn btn-danger btn-block')) !!}
            </div>
            <div class="col-sm-3">
                {{ Form::submit('Save Changes', ['class' => 'btn btn-success btn-block']) }}
            </div>
        </div>
    </div>
</div>

@endsection
