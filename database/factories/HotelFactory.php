<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class HotelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'hotel_name' => $this->faker->name(),
            'floor' => $this->faker->numberBetween(2, 20),
            'rooms_number' => $this->faker->numberBetween(2, 20),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
