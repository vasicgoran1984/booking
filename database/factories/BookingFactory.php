<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BookingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->numberBetween(1, 10),
            'room_name' => 'room1',
            'title' => $this->faker->titleMale,
            'start_date' => date('Y-m-d'),
            'end_date' => $this->faker->date('Y-m-d'),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
