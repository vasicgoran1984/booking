<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['web']], function(){
    Route::get('/all-booking', 'App\Http\Controllers\BookingController@index')->name('all-booking');
    Route::get('/users-booking', 'App\Http\Controllers\BookingController@usersBookingInfo')->name('users-booking');
    //Bookings
    Route::resource('bookings', \App\Http\Controllers\BookingController::class);
    //Hotels
    Route::resource('hotels', \App\Http\Controllers\HotelController::class);
});


