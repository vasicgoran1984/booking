<?php

namespace App\Repositories;

use App\Models\Booking;
use Illuminate\Support\Facades\DB;

class BookingRepository
{
    protected $booking;

    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    public function getAll()
    {
        return $this->booking
            ->get();
    }

    public function getByUserId($id)
    {
        return DB::table('bookings')
            ->join('users', 'users.id', '=', 'bookings.user_id')
            ->select('users.id', 'users.name', 'users.email', 'bookings.id as booking_id', 'bookings.room_name', 'bookings.title', 'bookings.start_date', 'bookings.end_date')
            ->where('bookings.user_id', $id)
            ->get();
    }

    public function delete($id)
    {
        $booking = $this->booking->find($id);

        $booking->delete();

        return $booking;
    }

    public function getBookingById($id) {

        return $this->booking
            ->find($id);
    }

    public function update($data, $id)
    {
        $booking = $this->booking->find($id);

        $booking->room_name  = $data['room_name'];
        $booking->title      = $data['title'];
        $booking->start_date = $data['start_date'];
        $booking->end_date   = $data['end_date'];

        $booking->update();

        return $booking;
    }

    public function save($data)
    {
        $booking = new $this->booking;

        $booking->user_id    = $data['user_id'];
        $booking->hotel_id   = $data['hotel_id'];
        $booking->room_name  = $data['room_name'];
        $booking->title      = $data['title'];
        $booking->start_date = $data['start_date'];
        $booking->end_date   = $data['end_date'];

        $booking->save();

        return $booking->fresh();
    }
}
