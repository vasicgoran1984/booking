<?php

namespace App\Repositories;

use App\Models\Hotel;

class HotelRepository
{
    protected $hotel;

    public function __construct(Hotel $hotel)
    {
        $this->hotel = $hotel;
    }

    public function getAll()
    {
        return $this->hotel
            ->get();
    }

    public function delete($id)
    {
        $hotel = $this->hotel->find($id);

        $hotel->delete();

        return $hotel;
    }

    public function getHotelById($id) {

        return $this->hotel
            ->find($id);
    }

    public function update($data, $id)
    {
        $hotel = $this->hotel->find($id);

        $hotel->hotel_name   = $data['hotel_name'];
        $hotel->floor        = $data['floor'];
        $hotel->rooms_number = $data['rooms_number'];

        $hotel->update();

        return $hotel;
    }

    public function save($data)
    {
        $hotel = new $this->hotel;

        $hotel->hotel_name   = $data['hotel_name'];
        $hotel->floor        = $data['floor'];
        $hotel->rooms_number = $data['rooms_number'];

        $hotel->save();

        return $hotel->fresh();
    }
}
