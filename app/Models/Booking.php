<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Booking extends Model
{
    use HasFactory;

    protected $table = 'bookings';

    public function users()
    {
        return $this->belongsTo('App\Models\User');
    }

}
