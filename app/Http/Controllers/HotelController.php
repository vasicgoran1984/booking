<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreHotelRequest;
use App\Http\Requests\UpdateHotelRequest;
use App\Models\Hotel;
use App\Services\HotelService;
use Illuminate\Http\Request;
use Exception;

class HotelController extends Controller
{
    protected $hotelService;

    /**
     * HotelController Constructor
     *
     * @param HotelService $hotelService
     *
     */
    public function __construct(HotelService $hotelService)
    {
        $this->hotelService = $hotelService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = ['status' => 200];

        try {
            $result['data'] = $this->hotelService->getAll();
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }

        return view('hotels.all-hotels')->withHotels($result['data']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('hotels.create-hotel');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreHotelRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only([
            'hotel_name',
            'floor',
            'rooms_number',
        ]);


        $result = ['status' => 200];

        try {
            $result['data'] = $this->hotelService->saveHotelData($data);
            $result['view'] = 'hotels.show-hotels';
            $result['success'] = 'New hotel successfully saved';

        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage(),
                'view' => 'hotels.create-hotel'
            ];
        }

        return view($result['view'],[
            'hotel' => (!empty($result['data'])) ? $result['data'] : '',
            'status' => $result['status'],
            'message' => (!empty($result['success'])) ? $result['success'] : $result['error'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hotel = $this->hotelService->getHotelById($id);

        return view('hotels.show-hotels')->withHotel($hotel);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = ['status' => 200];

        try {
            $result['data'] = $this->hotelService->getHotelById($id);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }

        return view('hotels.edit-hotel')->withHotel($result['data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateHotelRequest  $request
     * @param  \App\Models\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only([
            'hotel_name',
            'floor',
            'rooms_number',
        ]);

        $result = ['status' => 200];

        try {
            $result['data'] = $this->hotelService->updateHotel($data, $id);
            $result['view'] = 'hotels.show-hotels';
            $result['success'] = 'Hotel was updated successfully';
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage(),
                'view' => 'hotels.edit-hotel'
            ];
        }

        $result['hotel'] = $this->hotelService->getHotelById($id);

        return view($result['view'],[
            'hotel' => $result['hotel'],
            'status' => $result['status'],
            'message' => (!empty($result['success'])) ? $result['success'] : $result['error'],
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = ['status' => 200];

        try {
            $this->hotelService->deleteById($id);
            $result['success'] = 'Hotel was deleted successfully';
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }

        return redirect()->route('hotels.index')->with('status', $result['success']);

    }
}
