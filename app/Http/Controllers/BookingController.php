<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBookingRequest;
use App\Http\Requests\UpdateBookingRequest;
use App\Models\Booking;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use function response;
use Exception;
use App\Services\BookingService;
use Carbon\Carbon;
use App\Models\Hotel;

class BookingController extends Controller
{
    protected $bookingService;

    /**
     * BookingController Constructor
     *
     * @param BookingService $bookingService
     *
     */
    public function __construct(BookingService $bookingService)
    {
        $this->bookingService = $bookingService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = ['status' => 200];

        try {
            $result['data'] = $this->bookingService->getAll();
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }

        return view('bookings.all-bookings')->withBookings($result['data']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $hotels = Hotel::all();

        return view('bookings.create-booking')->withHotels($hotels);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBookingRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only([
            'room_name',
            'title',
            'hotel_id',
            'start_date',
            'end_date',
        ]);

        $data['user_id'] = Auth::user()->id;
        $result = ['status' => 200];

        try {
            $result['data'] = $this->bookingService->saveBookingData($data);
            $result['view'] = 'bookings.show-bookings';
            $result['success'] = 'New booking successfully saved';

        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage(),
                'view' => 'bookings.create-booking'
            ];
        }

        return view($result['view'],[
            'booking' => (!empty($result['data'])) ? $result['data'] : '',
            'status' => $result['status'],
            'message' => (!empty($result['success'])) ? $result['success'] : $result['error'],
            'hotels' => $hotels = Hotel::all()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking = $this->bookingService->getBookingById($id);

        return view('bookings.show-bookings')->withBooking($booking);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hotels = Hotel::all();

        $result = ['status' => 200];

        try {
            $result['data'] = $this->bookingService->getBookingById($id);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }

        $result['data']->start_date = Carbon::parse($result['data']->start_date)->format('Y-m-d');
        $result['data']->end_date = Carbon::parse($result['data']->end_date)->format('Y-m-d');

        return view('bookings.edit-booking')->withBooking($result['data'])->withHotels($hotels);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBookingRequest  $request
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = $request->only([
            'room_name',
            'title',
            'hotel_id',
            'start_date',
            'end_date',
        ]);

        $result = ['status' => 200];

        try {
            $this->bookingService->updateBooking($data, $id);
            $result['view'] = 'bookings.show-bookings';
            $result['success'] = 'Booking was updated successfully';
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage(),
                'view' => 'bookings.edit-booking'
            ];
        }
        $result['booking'] = $this->bookingService->getBookingById($id);

        return view($result['view'],[
            'booking' => $result['booking'],
            'status' => $result['status'],
            'message' => (!empty($result['success'])) ? $result['success'] : $result['error'],
            'hotels' => $hotels = Hotel::all()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = ['status' => 200];

        try {
            $result['data'] = $this->bookingService->deleteById($id);
            $result['success'] = 'Booking was deleted successfully';
        } catch (Exception $e) {
            $result = [
              'status' => 500,
              'error' => $e->getMessage()
            ];
        }

        return redirect()->route('bookings.index')->with('status', $result['success']);

    }

    public function showBookingByUser(Request $request)
    {
        $login = $request->validate([
            'email'    => 'required|string',
            'password' => 'required|string',
        ]);

        if (!Auth::attempt($login)) {
            return response(['message' => 'Invalid login credentials!']);
        }

        $user = Auth::user();
        $bookingByUser = $user->bookings;

        return response(['user' => Auth::user(), 'bookings' => $bookingByUser]);

    }

    public function showBookingInfoByUser(Request $request)
    {
        $result = Auth::guard('api')->user()->id;
        $results = Booking::select('id', 'user_id', 'room_name')->where('user_id', '=', $result)->get();

        return response(['user' => $results]);
    }

    public function usersBookingInfo()
    {
        $userId = Auth::user()->id;

        $result = ['status' => 200];

        try {
            $result['data'] = $this->bookingService->getByUserId($userId);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }

        return view('bookings.user-booking')->withBookings($result['data']);;
    }

    public function destroyBooking(Request $request)
    {
        $bookingId = $request->input('id');
        $result = ['status' => 200];

        try {
            $result['data'] = $this->bookingService->deleteById($bookingId);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);

    }
}
