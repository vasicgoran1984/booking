<?php

namespace App\Services;

use App\Models\Booking;
use Exception;
use App\Repositories\BookingRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;

class BookingService
{
    protected $bookingRepository;

    public function __construct(BookingRepository $bookingRepository)
    {
        $this->bookingRepository = $bookingRepository;
    }

    public function getAll()
    {
        return $this->bookingRepository->getAll();
    }

    public function getByUserId($id)
    {
        return $this->bookingRepository->getByUserId($id);
    }

    public function deleteById($id)
    {
        DB::beginTransaction();

        try {
            $booking = $this->bookingRepository->delete($id);
        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new \InvalidArgumentException('Unable to delete booking data');
        }

        DB::commit();

        return $booking;
    }

    public function getBookingById($id)
    {
        return $this->bookingRepository->getBookingById($id);
    }

    public function updateBooking($data, $id)
    {
        $validator = Validator::make($data, [
           'room_name' => 'required|min:3|max:15',
           'title' => 'required|min:3',
            'hotel_id' => 'required',
           'start_date' => 'required|date',
           'end_date' => 'required|date|after_or_equal:start_date',
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $booking = $this->bookingRepository->update($data, $id);

        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new InvalidArgumentException('Unable to update booking data');
        }

        DB::commit();

        return $booking;

    }

    public function saveBookingData($data)
    {
        $validator = Validator::make($data, [
            'room_name' => 'required|min:3|max:15',
            'title' => 'required|min:3',
            'hotel_id' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date',
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        return $this->bookingRepository->save($data);
    }
}
