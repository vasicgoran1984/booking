<?php

namespace App\Services;

use App\Models\Hotel;
use Exception;
use App\Repositories\HotelRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;

class HotelService
{
    protected $hotelRepository;

    public function __construct(HotelRepository $hotelRepository)
    {
        $this->hotelRepository = $hotelRepository;
    }

    public function getAll()
    {
        return $this->hotelRepository->getAll();
    }

    public function getByUserId($id)
    {
        return $this->hotelRepository->getHotelById($id);
    }

    public function deleteById($id)
    {
        DB::beginTransaction();

        try {
            $hotel = $this->hotelRepository->delete($id);
        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new \InvalidArgumentException('Unable to delete hotel data');
        }

        DB::commit();

        return $hotel;
    }

    public function getHotelById($id)
    {
        return $this->hotelRepository->getHotelById($id);
    }

    public function updateHotel($data, $id)
    {
        $validator = Validator::make($data, [
            'hotel_name' => 'required|min:3|max:15',
            'floor' => 'required',
            'rooms_number' => 'required',
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $hotel = $this->hotelRepository->update($data, $id);

        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new InvalidArgumentException('Unable to update hotel data');
        }

        DB::commit();

        return $hotel;
    }

    public function saveHotelData($data)
    {
        $validator = Validator::make($data, [
            'hotel_name' => 'required|min:3|max:15',
            'floor' => 'required',
            'rooms_number' => 'required',
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        return $this->hotelRepository->save($data);
    }
}
